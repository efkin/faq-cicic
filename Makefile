.PHONY: all
all: textile

.PHONY: textile
textile:
	asciidoc --verbose -a toc2 faqs-cicic-es.asciidoc; pandoc -s -S faqs-cicic-es.html -t textile -o faqs-cicic-es.textile; rm faqs-cicic-es.html
